package matrix.mapped;

import java.util.HashMap;
import java.util.Map;

import matrix.array2d.Matrix;

/**
 * matrix implementation using Maps
 * 
 * @author karolis
 *
 */
public class MappedMatrix {

	/**
	 * 2d Map
	 */
	private Map<Integer, Map<Integer, Integer>> matrixMap;
	
	private int size;
	
	/**
	 * default constructor
	 */
	public MappedMatrix() {
		Matrix tmp = new Matrix();
		tmp.generateSymmetricMatrix();
		mapMatrix(tmp);
	}
	
	/**
	 * map existing matrix
	 * 
	 * @param m existing matrix
	 */
	public MappedMatrix(Matrix m){
		mapMatrix(m);
	}
	
	/**
	 * puts matrix to map
	 * 
	 * @param m matrix
	 */
	private void mapMatrix(Matrix m) {
		matrixMap = new HashMap<Integer, Map<Integer,Integer>>();
		
		this.size = m.getSize();
		
		Map<Integer, Integer> tmp;
		
		for (int i = 0; i < m.getSize(); i++){
			for(int j = 0; j < m.getSize(); j++){
				if (m.getXY(j, i) != 0){
					if (!matrixMap.containsKey(i)){
						tmp = new HashMap<Integer, Integer>();
						matrixMap.put(i, tmp);
					}
					matrixMap.get(i).put(j, m.getXY(j, i));
				}
			}
		}
	}
	
	//TODO rmRowAndCol - can't be done with maps :(
	//Could ArrayList be the answer?

	/**
	 * get array of sums of lines
	 * @return array of sums of lines
	 */
	public int[] calculateRowSums() {
		int[] res = new int[this.size];
		
		for (int i = 0; i < this.size; i++){
			if (matrixMap.containsKey(i)){
				for (int j = 0; j < this.size; j++){
					if (matrixMap.get(i).containsKey(j))
						res[i] += matrixMap.get(i).get(j);
				}
			}
		}
		return res;
	}
	
	/**
	 * calculates determinant
	 */
	public long calculateDeterminant() {
		long res = 0;
		
		if (size == 1){
			res = matrixMap.get(0).get(0);
		}
		
		else if (size == 2){
			int a11;
			int a12;
			int a21;
			int a22;
			if (matrixMap.containsKey(0)){
				if (matrixMap.get(0).containsKey(0))
					a11 = matrixMap.get(0).get(0);
				else a11 = 0;
				if (matrixMap.get(0).containsKey(1))
					a12 = matrixMap.get(0).get(1);
				else a12 = 0;
			} else {
				a11 = 0;
				a12 = 0;
			}
			if (matrixMap.containsKey(1)){
				if (matrixMap.get(1).containsKey(0))
					a21 = matrixMap.get(1).get(0);
				else a21 = 0;
				if (matrixMap.get(1).containsKey(0))
					a22 = matrixMap.get(1).get(1);
				else a22 = 0;
			} else {
				a21 = 0;
				a22 = 0;
			}
			res = a11*a22 - a21*a12;
		}
		
		else {  //shit gets serious here :D
			//TODO the serious part
		}
		
		return res;
	}
	
	/*private void createSubMatrix(int exclRow, int exclCol) {
		//changing ranges should be enough...
		//NOT NEEDED
	}*/
	
	/*private void simplify() {
		//TODO
	}*/
}
