package matrix.array2d;

import java.util.concurrent.CountDownLatch;

/**
 * thread for calculating determinants of sub-matrices
 * 
 * @author karolis
 *
 */
public class DeterminantThread implements Runnable {

	/**
	 * matrix to be calculated
	 */
	protected Matrix matrix;
	
	/**
	 * number of thread
	 */
	protected int threadNumber;
	
	/**
	 * results array
	 */
	protected int[] results;
	
	/**
	 * count down latch
	 */
	protected CountDownLatch cdl;
	
	/**
	 * amount of deep threads
	 */
	private int threads = 0;

	/**
	 * amount of deeper threads
	 */
	private int presetDeepThreads = 0;
	
	/**
	 * constructor
	 * 
	 * @param matrix matrix to be calculated 
	 * @param threadNumber number of thread
	 * @param results results array
	 * @param cdl count down latch
	 */
	public DeterminantThread(Matrix matrix, 
			int threadNumber, 
			int[] results, 
			CountDownLatch cdl) {
		this.matrix = matrix;
		this.threadNumber = threadNumber;
		this.cdl = cdl;
		this.results = results;
	}
	
	/**
	 * constructor for deep threads (experimental)
	 * 
	 * @param matrix matrix to be calculated 
	 * @param threadNumber number of thread
	 * @param results results array
	 * @param cdl count down latch
	 * @param threads amount of deep threads
	 */
	public DeterminantThread(Matrix matrix, 
			int threadNumber, 
			int[] results, 
			CountDownLatch cdl,
			int threads) {
		this(matrix, threadNumber, results, cdl);
		this.threads = threads;
	}
	
	/**
	 * run runnable
	 */
	@Override
	public void run() {
		
		System.out.println("Determinant calculation thread " + threadNumber + " STARTED...");
		
		int res;
		if (threads == 1 || threads ==0){
			res = matrix.calculateDeterminant();
		} else {
			res = matrix.calculateDeterminant(threads, presetDeepThreads);
		}

		results[threadNumber] = res;
		
		System.out.println("Determinant calculation thread " + threadNumber + " RESULT: " + res);
		
		cdl.countDown();
	}

}
