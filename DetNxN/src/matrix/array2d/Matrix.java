package matrix.array2d;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Symmetrical matrix
 * 
 * @author karolis
 *
 */
public class Matrix {
	
	/**
	 * matrix - 2D array of bytes
	 */
	private byte[][] matrix;

	/**
	 * size of matrix
	 */
	private int size = 0;//n
	
	/**
	 * submatrix size (size-1)
	 */
	private int subSize = 0;
	
	/**
	 * probability (P) for 1
	 */
	private int prob = 0;
	
	/**
	 * more output text?
	 */
	public boolean verbose = false;

	
	Integer subMatrixCount = null;
	
	/**
	 * default constructor
	 */
	public Matrix() {
		this.size = 2;
		this.prob = 1;
		
		allocMatrix();
	}
	
	/**
	 * matrix constructor with probability ( for generate() )
	 * 
	 * @param size
	 * @param probability
	 */
	public Matrix(int size, int probability) {
		if (size < 1 || probability < 0)
			throw new IllegalArgumentException("Illegal matrix parameters");
		
		this.size = size;
		this.prob = probability;
		
		allocMatrix();
	}

	/**
	 * constructor
	 * 
	 * @param size
	 */
	public Matrix(int size) {
		if (size < 1)
			throw new IllegalArgumentException("Illegal matrix parameters, size = " + size);
		
		this.size = size;
		allocMatrix();
	}
	
	/**
	 * constructor with probability
	 * counts sub-matrices
	 * 
	 * @param size
	 * @param probability
	 * @param subMatrixCount
	 */
	public Matrix(int size, int probability, Integer subMatrixCount){
		this(size, probability);
		this.subMatrixCount = subMatrixCount;
	}
	
	/**
	 * constructor
	 * counts sub-matrices
	 * 
	 * @param size
	 * @param subMatrixCount
	 */
	public Matrix(int size, Integer subMatrixCount){
		this(size);
		this.subMatrixCount = subMatrixCount;
	}

	/**
	 * allocates NxN matrix, when N = size
	 */
	private void allocMatrix(){
		this.subSize = size - 1;
		matrix = new byte[size][size];
	}

	/**
	 * randomly generates symmetric matrix
	 */
	public void generateSymmetricMatrix() {
		if (prob == 0)
			throw new IllegalArgumentException("No probability for matrix is set");
		
		Random generator = new Random();
		
		if (this.verbose)
			System.out.println("GENERATING - " + size + "x" + size + " matrix");
		
		int a;
		
		//generate
		for (int x = 0; x < size; x++) {
			for (int y = x; y < size; y++) {
				a = generator.nextInt(size);
				if (a < prob) 
					matrix[x][y] = 1;
					else matrix[x][y] = 0;
				matrix[y][x] = matrix[x][y];
			}
		}
		
		if (this.verbose)
			System.out.println("SUCCESS - " + size + "x" + size + 
					" matrix generated");
	}
	
	
	/**
	 * calculate determinant with using setted amount of active threads
	 * 
	 * @param threads amount of threads to be used
	 * @return calculated determinant
	 */
	public int calculateDeterminant(int threads, int deepThreads){
		int res = 0;
		
		if (size == 1){
			res =matrix[0][0];
		}
		
		else if (size == 2){
			res = matrix[0][0]*matrix[1][1] - matrix[1][0]*matrix[0][1];
		}
		
		else {	//shit gets serious here :D
			
			//Queue of sub-matrixes
			LinkedBlockingQueue<Matrix> queue = new LinkedBlockingQueue<Matrix>();
			
			//Multipliers array
			int[] multipliers = new int[size];
			
			//Constructing queue
			for (int i = 0; i < size; i++){
				multipliers[i] = getXY(0, i);
				
				Matrix tmp = createSubMatrix(i, 0);
				
				try {
					queue.put(tmp);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			//System.out.println(size + " sub matrixes created");
			
			//Array of results
			int[] results = new int[size];
			
			
			
			//A FIXED pool of threads
			ExecutorService pool = Executors.newFixedThreadPool(threads);
			
			CountDownLatch cdl = new CountDownLatch(queue.size());
			
			int threadNumber = -1;
			
			//Threads are added to pool
			//TODO wait for others to complete before adding more - overflow if large matrices
			while (queue.iterator().hasNext()){
				try {
					threadNumber++;
					System.out.println("Determinant calculation thread " + threadNumber + " CHECKING...");
					
					if (multipliers[threadNumber] != 0){
						
						if (deepThreads == 0){
							pool.execute(new DeterminantThread(queue.take(), threadNumber, results, cdl));
						} else {
							pool.execute(new DeterminantThread(queue.take(), threadNumber, results, cdl, deepThreads));
						}
						System.out.println("Determinant calculation thread " + threadNumber + " ADDED to pool");
					} else {
						//Multiplier is 0 -> calculations skipped
						cdl.countDown();
						queue.take();
						System.out.println("   Mult. is 0");
						results[threadNumber] = 0;
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			System.out.println("---ALL THREADS ADDED---");
			
			//CountDownLatch allows main thread to continue only after ALL threads in THREAD pool are completed
			try {
				cdl.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			pool.shutdownNow();
			
			//Count result from those determinants
			System.out.println("Calculating final results...");
			/*System.out.println("   results array[" + results.length + "]:");
			for (int i = 0; i < results.length; i++)
				System.out.println("   [" + i + "] " + results[i]);*/
			for (int i = 0; i < results.length; i++){
				//System.out.println("res = " + res + " + " + sign(i) + " * " + multipliers[i] + " * " + results[i] + " =");
				res += sign(i) * multipliers[i] * results[i];
			}
			
			return res;
		}
		
		return res;
	}
	
	/**
	 * single threaded determinant calculation
	 * 
	 * @return calculated determinant
	 */
	public int calculateDeterminant() {
		int res = 0;
		
		if (size == 1){
			res = matrix[0][0];
		}
		
		else if (size == 2){
			res = matrix[0][0]*matrix[1][1] - matrix[1][0]*matrix[0][1];
		}
		
		else {	//shit gets serious here :D
			
			//Not sure if this is true...
			//TODO check if this is true
			if (checkForZeroLines()) {
				//System.out.println("   Zero lines detected [Det]");
				return 0;
			}
			
			
			int multiplier;
			boolean s = true;
			for (int i = 0; i < size; i++){
				 
				
				multiplier = getXY(0, i);
				
				if (multiplier == 1 && s && size > 1){
					//System.out.println("Trying to simplify (" + 0 + ", " + i +")=" + multiplier + " (size = " + size + "...");
					s = trySimplify(0, i);
					//multiplier = getXY(0, i);
					if (s) {
						//System.out.println("   Simplified [Det] :) - (" + 0 + ", " + i + ")");
						i = -1;
					}
				}
				
				if (size < 3){
					res = calculateDeterminant();
					return res;
				} else {
					if (multiplier != 0 && !s){
						Matrix tmp;
						//System.out.println("   Sub-Matrix created [Det] - (0, " + i + ") excluded");
						tmp = createSubMatrix(i, 0);
						res += sign(i) * multiplier * tmp.calculateDeterminant();
					}
				}
			}
			return res;
		}
		
		return res;
	}
	
	/*
	 * check whole matrix for simplifications
	 * USELESS
	 */
/*	public void trySimplifyAll() {
		
		int countInRow = 0;
		int whereIsTheOne = -1;
		boolean success = false;
		int i = 0;
		
		while (true){
			for (int j = 0; j < this.size; j++){
				if (matrix[i][j] == 1){
					success = trySimplify(i, j);
					break;	
				}
			}
			if (i == this.size - 1){
				break;
			}
			if (success) {
				i = 0;
			}
		}
	}*/
	
	/**
	 * checks if there are rows filled with zeroes in the matrix
	 * 
	 * @return
	 */
	private boolean checkForZeroLines() {
		for (int i = 0; i < this.size; i++){
			for (int j = 0; j < this.size; j++){
				if (matrix[i][j] == 1) break;
				if (j == this.size - 1)
					return true;//there is at least one row filled with zeroes
			}
		}
		return false;
	}
	
	/**
	 * if row and column has only one "1" - simplifies
	 * 
	 * @param row
	 * @param col
	 */
	private boolean trySimplify(int row, int col) {
		
		if (matrix[row][col] != 1)
			return false;
		
		for (int i = 0; i < this.size; i++){
			
			//check row
			if (matrix[row][i] == 1 && i != col){
				return false;
			}
			//check col
			if (matrix[i][col] == 1 && i != row){
				return false;
			}
		}
		
		//Now - simplify
		changeMatrix(createSubMatrix(col, row));
		return true;
	}
	
	
	/**
	 * change matrix
	 * 
	 * @param m matrix to 
	 */
	public void changeMatrix(Matrix m){
		this.matrix = m.getMatrix();
		this.size = m.getSize();
		this.subSize = m.subSize;
	}
	
	/**
	 * creates sub-matrix
	 * 
	 * @param exclCol column to be excluded
	 * @param exclRow row to be excluded
	 * @return sub-matrix
	 */
	public Matrix createSubMatrix(int exclCol, int exclRow) {
		Matrix res = new Matrix(subSize, subMatrixCount);
		
		int newRows = -1;
		int newCols = -1;
		
		for (int i = 0; i < this.size; i++){
			if (i == exclRow) i++;//skip excl. row
			if (i < this.size) {
				newRows++;
				
				newCols = -1;
				for (int j = 0; j < this.size; j++){
					if (j == exclCol) j++;//skip excl. col.
					if (j < this.size) {
						newCols++;
						try {
							/*System.out.println("[createSubMatrix]");
							System.out.println("   this.matrix[" + i + "][" + j + "] = "+ this.matrix[i][j]);*/
							res.setXYnoCheck(newRows, newCols, this.matrix[i][j]);
						} catch (ArrayIndexOutOfBoundsException e) {
							System.out.println("Array index out of bounds [createSubMatrix]");
							System.out.println("   this.size = " + this.size + " (x, y) = (" + i + ", " + j + ")");
							System.out.println("   sub-size  = " + this.subSize + " (x, y) = (" + newRows + ", " + newCols + ")");
						}
					}
				}
			}
		}
		
		if (verbose){
			System.out.println("CURRENT:");
			System.out.println(this.toString());
			System.out.println("NEW:   (without (" + exclRow + ", " + exclCol + ")");
			System.out.println(res.toString());
			if (subMatrixCount != null)
				subMatrixCount++;
				System.out.println("      Matrix no.: " + subMatrixCount);
		}
		
		return res;
	}
	
	/**
	 * 
	 * @return
	 */
	public int[] calculateRowSums() {
		int[] res = new int[size];
		for (int i = 0; i < size; i++){
			for (int j = 0; j < size; j++){
				res[i] += matrix[i][j];
			}
		}
		return res;
	}
	
	/**
	 * sign: 1 or -1
	 * 
	 * @param i number
	 * @return 1 if number is even, -1 if it's odd
	 */
	private int sign(int i) {
		if (i == 0)
			return 1;
		else if (i % 2 == 0)
			return 1;
		else return -1;
	}
	
	@Override
	public String toString() {
		if (matrix == null) return "***No matrix***";
		if (size > 50) return "***Matrix size > 50***";
		
		String matrixString = new String();
		
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				matrixString = matrixString + " " + matrix[x][y];
			}
			matrixString = matrixString + "\n";
		}
		
		return matrixString;
	}
	
	public int getXY(int x, int y) {
		int res = -1;
		
		try{
			res = this.matrix[x][y];
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Array index out of bounds [getXY]");
			System.out.println("   size = " + size + " (x, y) = (" + x + ", " + y + ")");
		}
		
		return res;
	}
	
	public void setXY(int x, int y, byte val){
		if (val == 0 || val == 1)
			this.matrix[x][y] = val;
			else throw new IllegalArgumentException("Value can only be 1 or 0");
	}
	
	public void setXYnoCheck(int x, int y, byte val){
		try{
			this.matrix[x][y] = val;
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Array index out of bounds [setXYnoCheck]");
			System.out.println("   size = " + size + " (x, y) = (" + x + ", " + y + ")");
		}
	}
	
	public byte[][] getMatrix() {
		return matrix;
	}

	public void setMatrix(byte[][] matrix) {
		this.matrix = matrix;
	}
	
	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	
}
