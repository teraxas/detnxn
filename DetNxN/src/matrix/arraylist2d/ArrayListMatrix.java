package matrix.arraylist2d;

import java.util.ArrayList;

import matrix.array2d.Matrix;

/**
 * matrix implementation using ArrayLists
 * 
 * @author karolis
 *
 */
public class ArrayListMatrix {

	/**
	 * Matrix implemented in array lists
	 */
	private ArrayList<ArrayList<Integer>> matrixArrayList;
	
	/**
	 * default constructor
	 * uses Matrix for generating 
	 * (which is stupid, but I'm too lazy to make generation for every implementation)
	 */
	public ArrayListMatrix() {
		Matrix tmp = new Matrix();
		tmp.generateSymmetricMatrix();
		createArrayList(tmp);
	}
	
	/**
	 * constructor
	 * creates ArrayListMatrix from existing Matrix
	 * @param m existing Matrix
	 */
	public ArrayListMatrix(Matrix m) {
		createArrayList(m);
	}
	
	/**
	 * creates ArrayList from existing Matrix
	 * @param m existing Matrix
	 */
	private void createArrayList(Matrix m){
		matrixArrayList = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i < m.getSize(); i++) {
			matrixArrayList.add(new ArrayList<Integer>());
			for (int j = 0; j < m.getSize(); j++){
				matrixArrayList.get(i).add(m.getXY(j, i));
			}
		}
	}

	/*private void simplify() {
		if (matrixArrayList == null)
			throw new NullPointerException("Matrix not initialised");
		
		for (ArrayList<Integer> row : matrixArrayList) {
			
		}
	}*/
	
	/**
	 * removes row and column of the matrix
	 * 
	 * @param row row
	 * @param col column
	 */
	public void rmRowAndCol(int row, int col) {
		if (matrixArrayList == null)
			throw new NullPointerException("Matrix not initialised");
		matrixArrayList.remove(row);
		for (ArrayList<Integer> row1 : matrixArrayList) {
			row1.remove(col);
		}
	}
}
