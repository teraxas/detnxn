package detnxn;

import java.util.Date;

import matrix.array2d.Matrix;


/**
 * 
 * @author karolis
 */
public class Main {

	/**
	 * matrix size (n)
	 */
	static int matrixSize = 20;
	
	/**
	 * probability (P) for "1"
	 */
	static int probability = 6;
	
	/**
	 * use threads?
	 * NOT RECOMENDED - possible overflow with large matrices
	 * works best WHEN used with small complicated matrices
	 */
	static boolean threaded = false;
	
	/**
	 * amount of threads used
	 */
	static int threads = 15;
	
	/**
	 * amount of deep threads
	 */
	static int deepThreads = 2;
	
	/**
	 * print results to screen
	 */
	static boolean printToScreen = true;
	
	/**
	 * print results to file?
	 */
	static boolean printToFile = true;
	
	/**
	 * counter for subMatrices
	 * NOT working :(
	 */
	static Integer subMatrixCount = 0;//TODO make a working counter for created sub matrices
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("+---------------------+");
		System.out.println("|Mathematical Analysis|");
		System.out.println("|***SPARSE MATRICES***|");
		System.out.println("|    by KJ and OK     |");
		System.out.println("+---------------------+");
		System.out.println();
		
		//Create and generate new matrix
		Matrix matrix = new Matrix(matrixSize, probability, subMatrixCount);
		matrix.generateSymmetricMatrix();
		
		//Attempt to print matrix (if size < 50)
		System.out.println(matrix.toString());
		
		//Attempt to simplify matrix
		/*System.out.println("---Trying to SIMPLIFY ALL...");
		matrix.trySimplifyAll();
		System.out.println("---DONE simplifying all!!!");*/
		
		//Calculate determinant
		long det;
		System.out.println("---Calculating DETERMINANT");
		long startTime = System.currentTimeMillis();
		if (threaded)
			det = matrix.calculateDeterminant(threads, deepThreads);
		else
			det = matrix.calculateDeterminant();
		long endTime = System.currentTimeMillis();
		System.out.println("---DONE calculating determinant");
		
		//Calculate duration of determinant calculation
		float seconds = (endTime - startTime) / 1000F;

		// *****should be commented/removed on determinant completion
		/*//COMPARISON
		//	 non-deep-threaded, threaded
		System.out.println("Starting non-deep-threaded calculation for comparison...");
		startTime = System.currentTimeMillis();
		long det3 = matrix.calculateDeterminant(threads, 0);
		endTime = System.currentTimeMillis();
		float seconds3 = (endTime - startTime) / 1000F;
		//   non-threaded
		System.out.println("Starting non-threaded calculation for comparison...");
		startTime = System.currentTimeMillis();
		long det2 = matrix.calculateDeterminant();
		endTime = System.currentTimeMillis();
		float seconds2 = (endTime - startTime) / 1000F;
		System.out.println("Results det = " + det + " = " + det3 + " = " + det2);
		System.out.println("deep-threaded (seconds): " + seconds);
		System.out.println("non-deep-threaded, threaded (seconds): "+ seconds3);
		System.out.println("normal   (seconds): " + seconds2);*/
		
		
		// *****should be commented/removed on determinant completion
		//PRINTS information to SCREEN
		if (printToScreen){
			System.out.println("");
			ResultPrinter resultsFile = new ResultPrinter();
			resultsFile.addLine("Sparse matrices");
			resultsFile.addLine(matrix.toString());
			resultsFile.addLine("Date", new Date().toString());
			resultsFile.addLine("Size", matrix.getSize());
			resultsFile.addLine("Probability for 1", probability);
			//resultsFile.addLine("Sub-matrices created", subMatrixCount);
			resultsFile.addLine("Determinant", det);
			resultsFile.addLine("Start", startTime);
			resultsFile.addLine("End", endTime);
			resultsFile.addLine("Duration (seconds)", seconds);
			if (threaded)
				resultsFile.addLine("Threads used", threads);
			
			resultsFile.printToScreen();
			
			//PRINTS information to FILE
			if (printToFile){
				resultsFile.printToFile();
				System.out.println("Results file (" + resultsFile.getFilename() + ") CREATED");
			}
		}
	}

}
