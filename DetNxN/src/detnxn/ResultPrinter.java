package detnxn;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Simplified results printing to file
 * 
 * @author karolis
 *
 */
public class ResultPrinter {

	/**
	 * Array list of lines
	 */
	private ArrayList<String> dataLines;
	
	/**
	 * filename of result file
	 */
	private String filename;

	/**
	 * default constructor
	 */
	public ResultPrinter() {
		dataLines = new ArrayList<String>();
		this.filename = "results.txt";
	}
	
	/**
	 * Constructor with filename
	 * 
	 * @param filename results filename
	 */
	public ResultPrinter(String filename) {
		dataLines = new ArrayList<String>();
		this.filename = filename;
	}
	
	/**
	 * add string to file
	 * 
	 * @param line
	 */
	public void addLine(String line) {
		dataLines.add(line + "\n");
	}
	
	/**
	 * add string with value to file
	 * 
	 * @param name name of value
	 * @param val value
	 */
	public void addLine(String name, int val) {
		String line;
		line = name + "<int>: " + val + "\n";
		dataLines.add(line);
	}
	
	/**
	 * add string with value to file
	 * 
	 * @param name name of value
	 * @param val value
	 */
	public void addLine(String name, long val) {
		String line;
		line = name + "<long>: " + val + "\n";
		dataLines.add(line);
	}
	
	/**
	 * add string with value to file
	 * 
	 * @param name name of value
	 * @param val value
	 */
	public void addLine(String name, String val) {
		String line;
		line = name + ": " + val + "\n";
		dataLines.add(line);
	}
	
	/**
	 * add string with value to file
	 * 
	 * @param name name of value
	 * @param val value
	 */
	public void addLine(String name, float val) {
		String line;
		line = name + "<float>: " + val + "\n";
		dataLines.add(line);
	}

	public void addLine(String name, double val) {
		String line;
		line = name + "<double>: " + val + "\n";
		dataLines.add(line);
	}
	
	/**
	 * print prepared lines to screen
	 */
	public void printToScreen() {
		for (String line : dataLines) {
			System.out.println(line);
		}
	}
	
	/**
	 * print to file
	 */
	public void printToFile() {
		try {
			FileWriter fStream = new FileWriter(filename);
			BufferedWriter out = new BufferedWriter(fStream);
			
			for (String line : dataLines) {
				out.write(line);
			}
			
			out.close();
		} catch (IOException e) {
			System.out.println("ERROR - file IO error");
		}
	}
	
	
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

}
